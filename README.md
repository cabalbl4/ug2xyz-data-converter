# UG 2 XYZ конвертер текстовых форматов с возможностью отображения данных

Автор Иван Вохмин (Ivan Vokhmin) <cabalbl4@gmail.com>
![Main windows XP](https://gitlab.com/cabalbl4/ug2xyz-data-converter/-/raw/master/xpexample.jpeg "Example")

Лицензия GNU GPLv3:

https://www.gnu.org/licenses/gpl-3.0.en.html

Репозиторий кода проекта: 

https://gitlab.com/cabalbl4/ug2xyz-data-converter


Linux => windows cross-build (full static build)

* `cd src`
* `docker run -ti -v "$(pwd):/project" syping/qt5-static-mingw /bin/sh`
* `export QT_SELECT=qt5-i686-w64-mingw32`
* `qmake-static qmake -qt=qt5-i686-w64-mingw32`

