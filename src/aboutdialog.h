#ifndef ABOUTDIALOG_H
#define ABOUTDIALOG_H

#include <QDialog>

namespace Ui {
class aboutDialog;
}

class AboutDialog : public QDialog
{
    Q_OBJECT

public:
    explicit
    AboutDialog(QWidget * parent = nullptr);
    ~AboutDialog();

private:
    Ui::aboutDialog * ui;
};

#endif // ABOUTDIALOG_H
