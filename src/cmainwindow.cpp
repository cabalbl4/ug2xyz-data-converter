/*
 * UG2XYZ format converter
 * Copyright (C) 2020  Ivan Vokhmin <cabalbl4@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdexcept>
#include <QFileDialog>
#include <QMessageBox>
#include <QTextStream>
#include <QDebug>
#include <QDesktopServices>
#include <QPen>

#include "cmainwindow.h"
#include "ui_cmainwindow.h"
#include "textparser.h"
#include "saveformatdialog.h"
#include "aboutdialog.h"
#include "graphswidget.h"
#include "qcustomplot/qcustomplot.h"


cMainWindow::cMainWindow(QWidget * parent) :
    QMainWindow(parent),
    ui(new Ui::cMainWindow)
{
    ui->setupUi(this);
    sortType_ = cXYZBeamer::sSortType::XREVERSE;
    ui->professionalModeFrame->setVisible(false);
    ui->prec_spinBox->setValue(DEFAULT_PRECISION_DIGITS_AFTER_ZERO);
    ui->prec_spinBox->setMaximum(MAXIMUM_DIGITS_AFTER_ZERO);
    fileFormat_ = SavedDialogResult::FORMAT_UNSET;
    updateFormatCaption();
}

cMainWindow::~cMainWindow()
{
    delete ui;
}

void
cMainWindow::on_closePushButton_clicked()
{
    this->close();
}

void
cMainWindow::on_openFilePushButton_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Открыть входной файл"),
        "",
        tr("Текстовые файлы (*.txt)"));

    if (fileName == "") return;

    QFile file(fileName);
    lastFileName_ = fileName;
    if (!file.open(QIODevice::ReadOnly) ) {
        QMessageBox msgBox;
        msgBox.setText(tr("Не могу открыть входной файл") );
        msgBox.exec();
        return;
    }
    QTextStream stream(&file);
    stream.setCodec(ui->inputEncodingComboBox->currentText().toLatin1().data());
    qDebug() << "Encoding for input: " << ui->inputEncodingComboBox->currentText().toLatin1().data();

    ui->inputPlainText->setPlainText(stream.readAll() );
    ui->outputPlainText->setPlainText("");
    fileFormat_ = SavedDialogResult::FORMAT_UNSET;
    updateFormatCaption();
    ui->convertPushButton->setEnabled(true);
    ui->savePushButton->setEnabled(true);
    ui->pushButtonSort->setEnabled(true);
    ui->pushButtonPlot->setEnabled(true);
    stream.seek(0);


    input_.clear();


    while (true) {
        QString line = stream.readLine();
        if (line.isNull() )
            break;
        else
            input_.append(line);
    }


    file.close();
} // cMainWindow::on_openFilePushButton_clicked

void
cMainWindow::on_convertPushButton_clicked()
{
    cTextParser p(input_);

    SaveFormatDialog saveFormatDialog(fileFormat_, this);

    saveFormatDialog.setModal(true);
    if (saveFormatDialog.exec() == 0) {
        qDebug() << "converting cancelled";
        return;
    }


    fileFormat_ = saveFormatDialog.getResult();
    updateFormatCaption();
    qDebug() << "selected format " << fileFormat_;

    cXYZBeamer b(p.getSet(), sortType_, fileFormat_ == SavedDialogResult::FORMAT_XYZ);
    ui->outputPlainText->setPlainText(b.result());
}

void
cMainWindow::on_savePushButton_clicked()
{
    if (fileFormat_ == SavedDialogResult::FORMAT_UNSET) {
        QMessageBox msgBox;
        msgBox.setText(tr("Сначала выберите формат, нажав кнопку \"Конвертировать\""));
        msgBox.setModal(true);
        msgBox.exec();
        return;
    }

    QString extension;

    switch (fileFormat_) {
        case SavedDialogResult::FORMAT_DAT:
            extension = "dat";
            break;
        case SavedDialogResult::FORMAT_XYZ:
        case SavedDialogResult::FORMAT_XYZ_INCH:
            extension = "xyz";
            break;
        default:
            throw std::runtime_error("Unknown extension");
    }

    QString name = lastFileName_;

    name.chop(3);
    name += extension;
    QString saveFileName =
      QFileDialog::getSaveFileName(0,
        tr("Сохранить результат"),
        name,
        tr("Файлы типа %1 (*.%1)").arg(extension));
    if (saveFileName == "") return;

    try {
        cTextParser p(input_);
        cXYZBeamer b(p.getSet(), sortType_, fileFormat_ == SavedDialogResult::FORMAT_XYZ);

        QFile file(saveFileName);
        if (!file.open(QIODevice::WriteOnly) ) {
            QMessageBox msgBox;
            msgBox.setText(tr("Не могу открыть выходной файл для записи") );
            msgBox.exec();
            return;
        }

        QTextStream stream(&file);
        stream << b.result();
        stream.flush();
        file.close();
    } catch (std::runtime_error & err) {
        QMessageBox msgBox;
        msgBox.setText(err.what());
        msgBox.exec();
    }
} // cMainWindow::on_savePushButton_clicked

void
cMainWindow::on_pushButtonSort_clicked()
{
    if (fileFormat_ == SavedDialogResult::FORMAT_UNSET) {
        QMessageBox msgBox;
        msgBox.setText(tr("Сначала выберите формат, нажав кнопку \"Конвертировать\""));
        msgBox.setModal(true);
        msgBox.exec();
        return;
    }
    ui->outputPlainText->setPlainText(QObject::tr("Сортирую...") );

    switch (ui->comboBoxAxisSelect->currentIndex() ) {
        case 0: {
            sortType_ = cXYZBeamer::sSortType::NOSORT;
            break;
        }
        case 1: {
            sortType_ = cXYZBeamer::sSortType::XFORWARD;
            break;
        }
        case 2: {
            sortType_ = cXYZBeamer::sSortType::XREVERSE;
            break;
        }
        case 3: {
            sortType_ = cXYZBeamer::sSortType::YFORWARD;
            break;
        }
        case 4: {
            sortType_ = cXYZBeamer::sSortType::YREVERSE;
            break;
        }
        case 5: {
            sortType_ = cXYZBeamer::sSortType::ZFORWARDBASEX;
            break;
        }
        case 6: {
            sortType_ = cXYZBeamer::sSortType::ZREVERSEBASEX;
            break;
        }
        case 7: {
            sortType_ = cXYZBeamer::sSortType::ZFORWARDBASEY;
            break;
        }
        case 8: {
            sortType_ = cXYZBeamer::sSortType::ZREVERSEBASEY;
            break;
        }
        case 9: {
            sortType_ = cXYZBeamer::sSortType::XY_IGNORE_Z_BASE_X_FORWARD;
            break;
        }
        case 10: {
            sortType_ = cXYZBeamer::sSortType::XY_IGNORE_Z_BASE_X_REVERSE;
            break;
        }
        case 11: {
            sortType_ = cXYZBeamer::sSortType::XY_IGNORE_Z_BASE_Y_FORWARD;
            break;
        }
        case 12: {
            sortType_ = cXYZBeamer::sSortType::XY_IGNORE_Z_BASE_Y_REVERSE;
            break;
        }

        case 13: {
            sortType_ = cXYZBeamer::sSortType::ZY_IGNORE_X_BASE_Y_FORWARD;
            break;
        }

        case 14: {
            sortType_ = cXYZBeamer::sSortType:: ZY_IGNORE_X_BASE_Y_REVERSE;
            break;
        }

        case 15: {
            sortType_ = cXYZBeamer::sSortType::ZX_IGNORE_Y_BASE_X_FORWARD;
            break;
        }

        case 16: {
            sortType_ = cXYZBeamer::sSortType::ZX_IGNORE_Y_BASE_X_REVERSE;
            break;
        }


        default:
            break;
    }
    cTextParser p(input_);
    cXYZBeamer b(p.getSet(), sortType_, fileFormat_ == SavedDialogResult::FORMAT_XYZ);
    ui->outputPlainText->setPlainText(b.result());
} // cMainWindow::on_pushButtonSort_clicked

void
cMainWindow::on_expertModeCheckBox_stateChanged(int _Checked)
{
    if (_Checked == 0) {
        ui->professionalModeFrame->setVisible(false);
    } else {
        ui->professionalModeFrame->setVisible(true);
    }
}

void
cMainWindow::on_prec_spinBox_valueChanged(int _Precision)
{
    CPrecision::setPrecision(_Precision);
}

void
cMainWindow::on_aboutButton_clicked()
{
    // QDesktopServices::openUrl(QUrl("https://gitlab.com/cabalbl4/ug2xyz-data-converter") );
    AboutDialog d;

    d.setModal(true);
    d.exec();
}

void
cMainWindow::updateFormatCaption()
{
    switch (fileFormat_) {
        case SavedDialogResult::FORMAT_UNSET:
            ui->saveFormatLabel->setText(tr("Рабочий формат не задан, нажмите \"Конвертировать\""));
            break;
        case SavedDialogResult::FORMAT_XYZ:
            ui->saveFormatLabel->setText(tr("Формат XYZ (Метрический)"));
            break;
        case SavedDialogResult::FORMAT_XYZ_INCH:
            ui->saveFormatLabel->setText(tr("Формат XYZ (Американский/дюймы)"));
            break;
        case SavedDialogResult::FORMAT_DAT:
            ui->saveFormatLabel->setText(tr("Формат DAT"));
            break;
    }
}

void
cMainWindow::on_pushButtonPlot_clicked()
{
    if (fileFormat_ == SavedDialogResult::FORMAT_UNSET) {
        QMessageBox msgBox;
        msgBox.setText(tr("Сначала выберите формат, нажав кнопку \"Конвертировать\""));
        msgBox.setModal(true);
        msgBox.exec();
        return;
    }

    cTextParser p(input_);
    cXYZBeamer b(p.getSet(), sortType_, fileFormat_ == SavedDialogResult::FORMAT_XYZ);
    auto resultVectors        = b.vectorResult();
    GraphsWidget * customPlot = new GraphsWidget(resultVectors.at(0), resultVectors.at(1), resultVectors.at(2));
    customPlot->setAttribute(Qt::WA_DeleteOnClose);
    customPlot->show();
    // create graph and assign data to it:
} // cMainWindow::on_pushButtonPlot_clicked
