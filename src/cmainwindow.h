/*
 * UG2XYZ format converter
 * Copyright (C) 2020  Ivan Vokhmin <cabalbl4@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CMAINWINDOW_H
#define CMAINWINDOW_H

#include <QMainWindow>
#include "textparser.h"
#include "saveformatdialog.h"

namespace Ui {
class cMainWindow;
}

class cMainWindow :  public QMainWindow
{
    Q_OBJECT

public:
    explicit
    cMainWindow(QWidget * parent = 0);
    ~cMainWindow();

private slots:
    void
    on_closePushButton_clicked();

    void
    on_openFilePushButton_clicked();

    void
    on_convertPushButton_clicked();

    void
    on_savePushButton_clicked();

    void
    on_pushButtonSort_clicked();

    void
    on_expertModeCheckBox_stateChanged(int _Checked);

    void
    on_prec_spinBox_valueChanged(int _Precision);

    void
    on_aboutButton_clicked();

    void
    on_pushButtonPlot_clicked();

private:
    void
    updateFormatCaption();

    Ui::cMainWindow * ui;
    QStringList input_;
    cXYZBeamer::sSortType::eSortType sortType_;
    QString lastFileName_;
    SavedDialogResult fileFormat_;
};

#endif // CMAINWINDOW_H
