#include "qcustomplot/qcustomplot.h"
#include <algorithm>
#include "graphswidget.h"
#include "ui_graphswidget.h"


GraphsWidget::GraphsWidget(QVector<double> x, QVector<double> y, QVector<double> z, QWidget * parent) :
    QWidget(parent),
    ui(new Ui::GraphsWidget)
{
    ui->setupUi(this);

    QVector<double> minimums {
        *std::min_element(x.begin(), x.end()),
        *std::min_element(y.begin(), y.end()),
        *std::min_element(z.begin(), z.end())
    };
    QVector<double> maximums {
        *std::max_element(x.begin(), x.end()),
        *std::max_element(y.begin(), y.end()),
        *std::max_element(z.begin(), z.end())
    };
    double min = *std::min_element(minimums.begin(), minimums.end());
    double max = *std::max_element(maximums.begin(), maximums.end());

    // qDebug() << resultVectors;
    ui->mainGraphWidget->addGraph();
    ui->mainGraphWidget->addGraph();
    ui->mainGraphWidget->addGraph();
    ui->mainGraphWidget->graph(0)->setPen(QPen(Qt::red));
    ui->mainGraphWidget->graph(1)->setPen(QPen(Qt::green));
    ui->mainGraphWidget->graph(2)->setPen(QPen(Qt::blue));
    ui->mainGraphWidget->graph(0)->setData(x, y);
    ui->mainGraphWidget->graph(1)->setData(y, z);
    ui->mainGraphWidget->graph(2)->setData(z, x);

    // give the axes some labels:
    ui->mainGraphWidget->xAxis->setLabel(tr("x - красный, y - зеленый,  z - синий"));
    ui->mainGraphWidget->yAxis->setLabel(tr("y - красный, z - зеленый,  x - синий"));
    // set axes ranges, so we see all data:
    ui->mainGraphWidget->xAxis->setRange(min - 1, max + 1);
    ui->mainGraphWidget->yAxis->setRange(min - 1, max + 1);
    ui->mainGraphWidget->replot();


    // Extra graphs
    ui->xWidget->addGraph();
    ui->yWidget->addGraph();
    ui->zWidget->addGraph();
    QVector<double> len(x.length());
    for (int i = 0; i < x.length(); i++) {
        len[i] = i;
    }
    ui->xWidget->yAxis->setLabel(tr("X"));
    ui->yWidget->yAxis->setLabel(tr("Y"));
    ui->zWidget->yAxis->setLabel(tr("Z"));
    ui->xWidget->xAxis->setLabel(tr("Точки"));
    ui->yWidget->xAxis->setLabel(tr("Точки"));
    ui->zWidget->xAxis->setLabel(tr("Точки"));
    ui->xWidget->graph(0)->setPen(QPen(Qt::red));
    ui->yWidget->graph(0)->setPen(QPen(Qt::green));
    ui->zWidget->graph(0)->setPen(QPen(Qt::blue));
    ui->xWidget->graph(0)->setData(len, x);
    ui->yWidget->graph(0)->setData(len, y);
    ui->zWidget->graph(0)->setData(len, z);

    ui->xWidget->xAxis->setRange(-1, x.length() + 1);
    ui->xWidget->yAxis->setRange(minimums[0] - 1, maximums[0] + 1);

    ui->yWidget->xAxis->setRange(-1, y.length() + 1);
    ui->yWidget->yAxis->setRange(minimums[1] - 1, maximums[1] + 1);

    ui->zWidget->xAxis->setRange(-1, z.length() + 1);
    ui->zWidget->yAxis->setRange(minimums[2] - 1, maximums[2] + 1);

    ui->xWidget->replot();
    ui->yWidget->replot();
    ui->zWidget->replot();
}

GraphsWidget::~GraphsWidget()
{
    delete ui;
}
