#ifndef GRAPHSWIDGET_H
#define GRAPHSWIDGET_H

#include <QWidget>
#include <QVector>

namespace Ui {
class GraphsWidget;
}

class GraphsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit
    GraphsWidget(QVector<double> x, QVector<double> y, QVector<double> z, QWidget * parent = nullptr);
    ~GraphsWidget();

private:
    Ui::GraphsWidget * ui;
};

#endif // GRAPHSWIDGET_H
