<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>QObject</name>
    <message>
        <location filename="../cmainwindow.cpp" line="120"/>
        <source>Сортирую...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SaveFormatDialog</name>
    <message>
        <location filename="../saveformatdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../saveformatdialog.ui" line="20"/>
        <source>Выберите формат сохранения</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../saveformatdialog.ui" line="26"/>
        <location filename="../saveformatdialog.ui" line="33"/>
        <source>RadioButton</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>cMainWindow</name>
    <message>
        <location filename="../cmainwindow.ui" line="14"/>
        <source>Конвертер txt -&gt; xyz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cmainwindow.ui" line="23"/>
        <source>Открыть</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cmainwindow.ui" line="33"/>
        <source>Конвертировать</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cmainwindow.ui" line="43"/>
        <source>Сохранить</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cmainwindow.ui" line="50"/>
        <source>Выход</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cmainwindow.ui" line="70"/>
        <source>Обрезать до знака после запятой при сравнении:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cmainwindow.ui" line="84"/>
        <source>Кодировка открываемого файла:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cmainwindow.ui" line="92"/>
        <source>Windows-1251</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cmainwindow.ui" line="97"/>
        <source>UTF-8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cmainwindow.ui" line="102"/>
        <source>KOI8-R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cmainwindow.ui" line="107"/>
        <source>KOI8-U</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cmainwindow.ui" line="112"/>
        <source>UTF-32</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cmainwindow.ui" line="139"/>
        <source>Не сортировать</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cmainwindow.ui" line="144"/>
        <source>Вдоль оси X - по возрастанию</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cmainwindow.ui" line="149"/>
        <source>Вдоль оси X - по убыванию</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cmainwindow.ui" line="154"/>
        <source>Вдоль оси Y -  по возрастанию</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cmainwindow.ui" line="159"/>
        <source>Вдоль оси Y -  по убыванию</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cmainwindow.ui" line="164"/>
        <source>Вдоль оси Z - по возрастанию, опорная ось X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cmainwindow.ui" line="169"/>
        <source>Вдоль оси Z - по убыванию, опорная ось X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cmainwindow.ui" line="174"/>
        <source>Вдоль оси Z - по возрастанию, опорная ось Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cmainwindow.ui" line="179"/>
        <source>Вдоль оси Z - по убыванию, опорная ось Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cmainwindow.ui" line="190"/>
        <source>Сортировать</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cmainwindow.ui" line="221"/>
        <source>Режим эксперта</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cmainwindow.ui" line="228"/>
        <source>Помощь</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cmainwindow.cpp" line="35"/>
        <source>Открыть входной файл</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cmainwindow.cpp" line="37"/>
        <source>Текстовые файлы (*.txt)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cmainwindow.cpp" line="45"/>
        <source>Не могу открыть входной файл</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cmainwindow.cpp" line="91"/>
        <source>Сохранить результат</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cmainwindow.cpp" line="93"/>
        <source>Файлы xyz (*.xyz)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cmainwindow.cpp" line="106"/>
        <source>Не могу открыть выходной файл для записи</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
