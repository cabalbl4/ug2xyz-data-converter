/*
 * UG2XYZ format converter
 * Copyright (C) 2020  Ivan Vokhmin <cabalbl4@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "precision.h"

#include <math.h>

#include <QString>

double
CPrecision::correctPrecision(double _What)
{
    //    long long int l_Divider = (long long int)pow(10,m_Precision);
    //    long long int l_Temp = (_What*l_Divider);
    //    double l_Result = l_Temp/l_Divider;
    //    qDebug() <<"Precision:" <<m_Precision <<" TRUNCATE "<<QString::number(_What,'g',12) <<" TO "<<QString::number(l_Result,'g',12);
    //    return l_Result;


    QString l_Double = QString::number(_What, 'g', MAXIMUM_DIGITS_AFTER_ZERO);

    if (!l_Double.contains('.')) return _What;

    double l_Result = 0;
    if (m_Precision == 0) {
        l_Result = l_Double.left(l_Double.indexOf('.')).toDouble();
    } else if (m_Precision > 0) {
        l_Result = l_Double.left(l_Double.indexOf('.') + 1 + m_Precision).toDouble();
    } else if (m_Precision < 0) {
        l_Result = _What;
    }

    // qDebug() <<"Precision:" <<m_Precision <<" TRUNCATE "<<QString::number(_What,'g',12) <<" TO "<<QString::number(l_Result,'g',12);
    return l_Result;
}

CPrecision::CPrecision()
{ }

int CPrecision::m_Precision = DEFAULT_PRECISION_DIGITS_AFTER_ZERO;
