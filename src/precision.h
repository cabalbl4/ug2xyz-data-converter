/*
 * UG2XYZ format converter
 * Copyright (C) 2020  Ivan Vokhmin <cabalbl4@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#define DEFAULT_PRECISION_DIGITS_AFTER_ZERO 5
#define MAXIMUM_DIGITS_AFTER_ZERO           12

#include <QDebug>
class CPrecision
{
public:
    static void
    setPrecision(int _DigitsAfterZero){ m_Precision = _DigitsAfterZero; }

    static int
    precision(){ return m_Precision; }

    // Обрезать символы после указанного (становятся нулями)
    static double
    correctPrecision(double _What);

private:
    CPrecision();
    static int m_Precision;
};
