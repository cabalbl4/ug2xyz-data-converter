/*
 * UG2XYZ format converter
 * Copyright (C) 2020  Ivan Vokhmin <cabalbl4@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "saveformatdialog.h"
#include "ui_saveformatdialog.h"
#include <QDebug>

SaveFormatDialog::SaveFormatDialog(SavedDialogResult initialResult, QWidget * parent) :
    QDialog(parent),
    ui(new Ui::SaveFormatDialog)
{
    ui->setupUi(this);
    switch (initialResult) {
        case SavedDialogResult::FORMAT_DAT:
            ui->formatDATRadioButton->setChecked(true);
            break;
        case SavedDialogResult::FORMAT_XYZ:
            ui->formatXYZRadioButton->setChecked(true);
            break;
        case SavedDialogResult::FORMAT_XYZ_INCH:
            ui->formatXYZRadioButton->setChecked(true);
            ui->inchModeCheckBox->setChecked(true);
            break;
        default:
            break;
    }


    result_ = (initialResult == SavedDialogResult::FORMAT_UNSET ? SavedDialogResult::FORMAT_XYZ : initialResult);
}

SaveFormatDialog::~SaveFormatDialog()
{
    delete ui;
}

SavedDialogResult
SaveFormatDialog::getResult()
{
    return result_;
}

void
SaveFormatDialog::on_inchModeCheckBox_stateChanged(int state)
{
    if (ui->formatXYZRadioButton->isChecked()) {
        if (Qt::Unchecked == state) {
            qDebug() << "Set format" << SavedDialogResult::FORMAT_XYZ;
            result_ = (SavedDialogResult::FORMAT_XYZ);
        } else {
            result_ = (SavedDialogResult::FORMAT_XYZ_INCH);
            qDebug() << "Set format" << SavedDialogResult::FORMAT_XYZ_INCH;
        }
    }
}

void
SaveFormatDialog::on_formatDATRadioButton_toggled(bool checked)
{
    if (checked) {
        result_ = (SavedDialogResult::FORMAT_DAT);
        qDebug() << "Set format" << SavedDialogResult::FORMAT_DAT;
    }
}

void
SaveFormatDialog::on_formatXYZRadioButton_toggled(bool checked)
{
    if (checked) {
        if (Qt::Unchecked == ui->inchModeCheckBox->checkState()) {
            result_ = (SavedDialogResult::FORMAT_XYZ);
            qDebug() << "Set format" << SavedDialogResult::FORMAT_XYZ;
        } else {
            result_ = (SavedDialogResult::FORMAT_XYZ_INCH);
            qDebug() << "Set format" << SavedDialogResult::FORMAT_XYZ_INCH;
        }
    }
}
