/*
 * UG2XYZ format converter
 * Copyright (C) 2020  Ivan Vokhmin <cabalbl4@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SAVEFORMATDIALOG_H
#define SAVEFORMATDIALOG_H

#include <QDialog>

enum SavedDialogResult {
    FORMAT_UNSET    = 0,
    FORMAT_XYZ      = 1,
    FORMAT_XYZ_INCH = 2,
    FORMAT_DAT      = 3,
};

namespace Ui {
class SaveFormatDialog;
}

class SaveFormatDialog : public QDialog
{
    Q_OBJECT

public:
    explicit
    SaveFormatDialog(SavedDialogResult initialResult, QWidget * parent = nullptr);
    ~SaveFormatDialog();

    SavedDialogResult
    getResult();

private:
    SavedDialogResult result_;

signals:
    void
    onDialogResult(SavedDialogResult result);

private slots:
    void
    on_inchModeCheckBox_stateChanged(int arg1);

    void
    on_formatDATRadioButton_toggled(bool checked);

    void
    on_formatXYZRadioButton_toggled(bool checked);

private:
    Ui::SaveFormatDialog * ui;
};

#endif // SAVEFORMATDIALOG_H
