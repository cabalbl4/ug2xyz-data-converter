/*
 * UG2XYZ format converter
 * Copyright (C) 2020  Ivan Vokhmin <cabalbl4@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "textparser.h"
#include <QDebug>
#include <stdexcept>
#include <QVector>

cTextParser::~cTextParser()
{
    foreach(sCoordsSet * c, set_)
    {
        delete c;
    }
}

void
cTextParser::parse()
{
    for (int i = 0; i < slist_.size(); i++) {
        // New format
        if (slist_.at(i).contains("[mm]")) {
            QString buffer = slist_.at(i).mid(slist_.at(i).indexOf("[mm]") + 4, -1);
            buffer = buffer.replace(' ', "");
            QStringList coords = buffer.split('\t');
            // First one is empty
            if (coords.length() != 4) {
                throw  std::runtime_error("Unexpected number of coordinates in new format");
            }
            QString xCoord(coords.at(1));
            QString yCoord(coords.at(2));
            QString zCoord(coords.at(3));

            sCoordsSet * s = new sCoordsSet(xCoord, yCoord, zCoord);
            set_.append(s);
        } else if (slist_.at(i).contains("XC =") ) {
            QString xCoord;
            QString yCoord;
            QString zCoord;

            for (int k = slist_.at(i).indexOf("X = ") + 4; k < slist_.at(i).length(); k++) {
                if (k == 0) throw std::runtime_error("Parsing error");
                if (slist_.at(i).at(k) == '\n') break;
                xCoord += slist_.at(i).at(k);
            }
            xCoord = xCoord.replace(" ", "");


            for (int k = slist_.at(i + 1).indexOf("Y = ") + 4; k < slist_.at(i + 1).length(); k++) {
                if (k == 0) throw std::runtime_error("Parsing error");
                if (slist_.at(i + 1).at(k) == '\n') break;
                yCoord += slist_.at(i + 1).at(k);
            }
            yCoord = yCoord.replace(" ", "");


            for (int k = slist_.at(i + 2).indexOf("Z = ") + 4; k < slist_.at(i + 2).length(); k++) {
                if (k == 0) throw std::runtime_error("Parsing error");
                if (slist_.at(i + 2).at(k) == '\n') break;
                zCoord += slist_.at(i + 2).at(k);
            }
            zCoord = zCoord.replace(" ", "");

            sCoordsSet * s = new sCoordsSet(xCoord, yCoord, zCoord);
            set_.append(s);
        }
    }
} // cTextParser::parse

cXYZBeamer::cXYZBeamer(const QList<sCoordsSet *> &set, sSortType::eSortType esort, bool addXYZString)
{
    cBucketCan b(set, esort);

    beam(b.result(), addXYZString);
}

void
cXYZBeamer::beam(const QList<sCoordsSet *> &set, bool addXYZString)
{
    int idx = 0;

    result_.clear();
    vectorResult_.clear();
    QVector<double>
    vX(set.length()), vY(set.length()), vZ(set.length());

    if (addXYZString) {
        result_ = "XYZ        METRIC\n";
    }

    foreach(sCoordsSet * s, set)
    {
        result_ += QString("   %1   %2   %3\n").arg(s->x_, s->y_, s->z_);
        vX[idx]  = s->x_.toDouble();
        vY[idx]  = s->y_.toDouble();
        vZ[idx]  = s->z_.toDouble();
        idx      = idx + 1;
    }
    vectorResult_.push_back(vX);
    vectorResult_.push_back(vY);
    vectorResult_.push_back(vZ);
    result_.chop(1);
}

QList<sCoordsSet *>
cBucket::sort(cBucket::sSortType::eSortType direction,
  eAxisName                                 sortableAxis)
{
    if (bucketNodes_.size() < 2) return bucketNodes_;

    // qDebug() << "BUCKES SIZE" << bucketNodes_.size();
    QList<sCoordsSet *> sortSet = bucketNodes_;
    bool isSwaped = true;

    while (isSwaped) {
        isSwaped = false;
        for (int i = 1; i < sortSet.size(); i++) {
            if (sortableAxis == XAXIS) {
                if (CPrecision::correctPrecision(sortSet.at(i - 1)->x_.toDouble()) >
                  CPrecision::correctPrecision(sortSet.at(i)->x_.toDouble())
                )
                {
                    sortSet.swap(i - 1, i);
                    isSwaped = true;
                    break;
                }
            }

            if (sortableAxis == YAXIS) {
                if (CPrecision::correctPrecision(sortSet.at(i - 1)->y_.toDouble()) >
                  CPrecision::correctPrecision(sortSet.at(i)->y_.toDouble())
                )
                {
                    sortSet.swap(i - 1, i);
                    isSwaped = true;
                    break;
                }
            }

            if (sortableAxis == ZAXIS) {
                if (CPrecision::correctPrecision(sortSet.at(i - 1)->z_.toDouble()) >
                  CPrecision::correctPrecision(sortSet.at(i)->z_.toDouble())
                )
                {
                    sortSet.swap(i - 1, i);
                    isSwaped = true;
                    break;
                }
            }
        }
    }

    if (direction == cBucket::sSortType::ASC) {
        return sortSet;
    } else {
        QList<sCoordsSet *> reverseSet;
        for (int i = ( sortSet.size() - 1 ); i > -1; i--) {
            reverseSet.append(sortSet.at(i)  );
        }
        return reverseSet;
    }
} // cBucket::sort

cBucketCan::cBucketCan(QList<sCoordsSet *> nodes, cXYZBeamer::sSortType::eSortType sortType)
{
    if (sortType == cXYZBeamer::sSortType::NOSORT) {
        result_ = nodes;
        return;
    }


    if ( ( sortType == cXYZBeamer::sSortType::XY_IGNORE_Z_BASE_X_FORWARD) ||
      ( sortType == cXYZBeamer::sSortType::XY_IGNORE_Z_BASE_X_REVERSE ) ||
      ( sortType == cXYZBeamer::sSortType::XY_IGNORE_Z_BASE_Y_FORWARD) ||
      ( sortType == cXYZBeamer::sSortType::XY_IGNORE_Z_BASE_Y_REVERSE ) )
    {
        // Особые алгоритмы для оси XY
        result_ = xyAxisSpecialSort(nodes, sortType);
        return;
    }

    if ( ( sortType == cXYZBeamer::sSortType::ZFORWARDBASEX) ||
      ( sortType == cXYZBeamer::sSortType::ZREVERSEBASEX ) ||
      ( sortType == cXYZBeamer::sSortType::ZFORWARDBASEY) ||
      ( sortType == cXYZBeamer::sSortType::ZREVERSEBASEY ) )
    {
        // Особые алгоритмы для оси Z, в этой функции более не обрабатывается
        result_ = zAxisSpecialSort(nodes, sortType);
        return;
    }

    if (( sortType == cXYZBeamer::sSortType::ZY_IGNORE_X_BASE_Y_FORWARD) ||
      ( sortType == cXYZBeamer::sSortType::ZX_IGNORE_Y_BASE_X_FORWARD) ||
      ( sortType == cXYZBeamer::sSortType::ZY_IGNORE_X_BASE_Y_REVERSE) ||
      ( sortType == cXYZBeamer::sSortType::ZX_IGNORE_Y_BASE_X_REVERSE) )
    {
        result_ = zyzxAxisSpecialSort(nodes, sortType);
        return;
    }

    QList<double> buckets;
    QList<cBucket> bucketNodes;

    if ( ( sortType == cXYZBeamer::sSortType::XFORWARD ) ||
      ( sortType == cXYZBeamer::sSortType::XREVERSE ) )
    {
        foreach(sCoordsSet * node, nodes)
        {
            if (buckets.contains(CPrecision::correctPrecision(node->x_.toDouble()))) continue;
            buckets.append(CPrecision::correctPrecision(node->x_.toDouble()) );
        }

        QList<double> sortBuckets = buckets;

        qSort(sortBuckets.begin(), sortBuckets.end() );

        foreach(double bucket, sortBuckets)
        {
            cBucket b;

            foreach(sCoordsSet * node, nodes)
            {
                if (CPrecision::correctPrecision(node->x_.toDouble()) == bucket) {
                    b.add(node);
                }
            }

            bucketNodes.append(b);
        }
    }


    if ( ( sortType == cXYZBeamer::sSortType::YFORWARD ) ||
      ( sortType == cXYZBeamer::sSortType::YREVERSE ) )
    {
        foreach(sCoordsSet * node, nodes)
        {
            if (buckets.contains(CPrecision::correctPrecision(node->y_.toDouble()) ) ) continue;
            buckets.append(CPrecision::correctPrecision(node->y_.toDouble()) );
        }

        QList<double> sortBuckets = buckets;

        qSort(sortBuckets.begin(), sortBuckets.end() );

        foreach(double bucket, sortBuckets)
        {
            cBucket b;

            foreach(sCoordsSet * node, nodes)
            {
                if (CPrecision::correctPrecision(node->y_.toDouble()) == bucket) {
                    b.add(node);
                }
            }

            bucketNodes.append(b);
        }
    }


    bool asc = true;

    QList<sCoordsSet *> result;

    if ( ( sortType == cXYZBeamer::sSortType::XREVERSE ) ||
      ( sortType == cXYZBeamer::sSortType::YREVERSE )
    )
    {
        QList<cBucket> rbuckets;
        rbuckets.append(bucketNodes);

        bucketNodes.clear();

        for (int i = ( rbuckets.size() - 1 ); i > -1; i--) {
            bucketNodes.append(rbuckets.at(i) );
        }
    }

    // qDebug()<<bucketNodes.size();


    foreach(cBucket b, bucketNodes)
    {
        if (asc) {
            result.append(

                b.sort(cBucket::sSortType::DESC, ZAXIS)

            );
        } else {
            result.append(

                b.sort(cBucket::sSortType::ASC, ZAXIS)

            );
        }
        asc = !asc;
    }

    result_ = result;
};


QList<sCoordsSet *>
cBucketCan::zAxisSpecialSort(QList<sCoordsSet *> nodes,
  cXYZBeamer::sSortType::eSortType               sortType)
{
    QList<double> bucketsCoords;
    QList<cBucket> buckets;

    foreach(sCoordsSet * node, nodes)
    {
        if (!bucketsCoords.contains(CPrecision::correctPrecision(node->z_.toDouble()) ) ) {
            bucketsCoords.append(CPrecision::correctPrecision(node->z_.toDouble()) );
        }
    }

    qSort(bucketsCoords.begin(), bucketsCoords.end() );

    foreach(double bucket, bucketsCoords)
    {
        cBucket b;

        foreach(sCoordsSet * node, nodes)
        {
            if (CPrecision::correctPrecision(node->z_.toDouble()) == bucket) {
                b.add(node);
            }
        }

        buckets.append(b);
    }


    if ( ( sortType == cXYZBeamer::sSortType::ZREVERSEBASEX ) ||
      ( sortType == cXYZBeamer::sSortType::ZREVERSEBASEY )
    )
    {
        QList<cBucket> rbuckets;
        rbuckets.append(buckets);

        buckets.clear();

        for (int i = ( rbuckets.size() - 1 ); i > -1; i--) {
            buckets.append(rbuckets.at(i) );
        }
    }


    bool asc = true;
    QList<sCoordsSet *> result;

    foreach(cBucket b, buckets)
    {
        switch (sortType) {
            case (cXYZBeamer::sSortType::ZFORWARDBASEX):
            case (cXYZBeamer::sSortType::ZREVERSEBASEX): {
                if (asc) {
                    result.append(

                        b.sort(cBucket::sSortType::DESC, XAXIS)

                    );
                } else {
                    result.append(

                        b.sort(cBucket::sSortType::ASC, XAXIS)

                    );
                }
                asc = !asc;
                break;
            }
            case (cXYZBeamer::sSortType::ZFORWARDBASEY):
            case (cXYZBeamer::sSortType::ZREVERSEBASEY): {
                if (asc) {
                    result.append(

                        b.sort(cBucket::sSortType::DESC, YAXIS)

                    );
                } else {
                    result.append(

                        b.sort(cBucket::sSortType::ASC, YAXIS)

                    );
                }
                asc = !asc;
                break;
            }
            default: {
                throw -1;
                break;
            }
        }
    }
    return result;
} // cBucketCan::zAxisSpecialSort

QList<sCoordsSet *>
cBucketCan::xyAxisSpecialSort(QList<sCoordsSet *> nodes, cXYZBeamer::sSortType::eSortType sortType)
{
    QList<double> bucketsCoords;
    QList<cBucket> buckets;
    bool mainAxisIsX = sortType == cXYZBeamer::sSortType::XY_IGNORE_Z_BASE_X_FORWARD ||
      sortType == cXYZBeamer::sSortType::XY_IGNORE_Z_BASE_X_REVERSE;

    bool isForward = sortType == cXYZBeamer::sSortType::XY_IGNORE_Z_BASE_X_FORWARD ||
      sortType == cXYZBeamer::sSortType::XY_IGNORE_Z_BASE_Y_FORWARD;

    foreach(sCoordsSet * node, nodes)
    {
        if (!bucketsCoords.contains(CPrecision::correctPrecision(
              mainAxisIsX ? node->x_.toDouble() : node->y_.toDouble()) ) )
        {
            bucketsCoords.append(CPrecision::correctPrecision(mainAxisIsX ? node->x_.toDouble() : node->y_.toDouble()) );
        }
    }

    qSort(bucketsCoords.begin(), bucketsCoords.end() );

    foreach(double bucket, bucketsCoords)
    {
        cBucket b;

        foreach(sCoordsSet * node, nodes)
        {
            if (CPrecision::correctPrecision(mainAxisIsX ? node->x_.toDouble() : node->y_.toDouble()) ==
              bucket)
            {
                b.add(node);
            }
        }

        buckets.append(b);
    }

    if (!isForward) {
        QList<cBucket> rbuckets;
        rbuckets.append(buckets);

        buckets.clear();

        for (int i = ( rbuckets.size() - 1 ); i > -1; i--) {
            buckets.append(rbuckets.at(i) );
        }
    }

    QList<sCoordsSet *> result;
    bool asc = true;

    foreach(cBucket b, buckets)
    {
        result.append(
            b.sort(asc ? cBucket::sSortType::DESC : cBucket::sSortType::ASC, mainAxisIsX ? YAXIS : XAXIS)
        );
        asc = !asc;
    }
    return result;
} // cBucketCan::xyAxisSpecialSort

QList<sCoordsSet *>
cBucketCan::zyzxAxisSpecialSort(QList<sCoordsSet *> nodes, cXYZBeamer::sSortType::eSortType sortType)
{
    //    ZY_IGNORE_X_BASE_Y_FORWARD,
    //    ZY_IGNORE_X_BASE_Y_REVERSE,
    //    ZX_IGNORE_Y_BASE_X_FORWARD,
    //    ZX_IGNORE_Y_BASE_X_REVERSE,


    QList<double> bucketsCoords;
    QList<cBucket> buckets;
    bool mainAxisIsX = sortType == cXYZBeamer::sSortType::ZX_IGNORE_Y_BASE_X_FORWARD ||
      sortType == cXYZBeamer::sSortType:: ZX_IGNORE_Y_BASE_X_REVERSE;

    bool isForward = sortType == cXYZBeamer::sSortType::ZY_IGNORE_X_BASE_Y_FORWARD ||
      sortType == cXYZBeamer::sSortType::ZX_IGNORE_Y_BASE_X_FORWARD;

    foreach(sCoordsSet * node, nodes)
    {
        if (!bucketsCoords.contains(CPrecision::correctPrecision(
              mainAxisIsX ? node->x_.toDouble() : node->y_.toDouble()) ) )
        {
            bucketsCoords.append(CPrecision::correctPrecision(mainAxisIsX ? node->x_.toDouble() : node->y_.toDouble()) );
        }
    }

    qSort(bucketsCoords.begin(), bucketsCoords.end() );

    foreach(double bucket, bucketsCoords)
    {
        cBucket b;

        foreach(sCoordsSet * node, nodes)
        {
            if (CPrecision::correctPrecision(mainAxisIsX ? node->x_.toDouble() : node->y_.toDouble()) ==
              bucket)
            {
                b.add(node);
            }
        }

        buckets.append(b);
    }

    if (!isForward) {
        QList<cBucket> rbuckets;
        rbuckets.append(buckets);

        buckets.clear();

        for (int i = ( rbuckets.size() - 1 ); i > -1; i--) {
            buckets.append(rbuckets.at(i) );
        }
    }

    QList<sCoordsSet *> result;
    bool asc = true;

    foreach(cBucket b, buckets)
    {
        result.append(
            b.sort(asc ? cBucket::sSortType::DESC : cBucket::sSortType::ASC, ZAXIS)
        );
        asc = !asc;
    }
    return result;
} // cBucketCan::zAxisSpecialSort
