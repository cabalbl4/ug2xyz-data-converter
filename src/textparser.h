﻿/*
 * UG2XYZ format converter
 * Copyright (C) 2020  Ivan Vokhmin <cabalbl4@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TEXTPARSER_H
#define TEXTPARSER_H

#include <QStringList>
#include <QList>
#include <QVector>

#include "precision.h"

struct sCoordsSet {
    sCoordsSet (QString& x, QString& y, QString& z) : x_(x), y_(y), z_(z){ }

    QString x_;
    QString y_;
    QString z_;
};


class cTextParser
{
public:
    cTextParser(const QStringList& list) : slist_(list){ parse(); }

    ~cTextParser();

    void
    parse();

    QList<sCoordsSet *>
    getSet(){ return set_; }

private:
    QList<sCoordsSet *> set_;
    QStringList slist_;
};

class cXYZBeamer : public QObject
{
    Q_OBJECT

public:
    cXYZBeamer(){ }

    virtual ~cXYZBeamer(){ }

    struct sSortType {
        enum eSortType {
            NOSORT,
            XFORWARD,
            XREVERSE,
            YFORWARD,
            YREVERSE,
            ZFORWARDBASEX,
            ZREVERSEBASEX,
            ZFORWARDBASEY,
            ZREVERSEBASEY,
            XY_IGNORE_Z_BASE_X_FORWARD,
            XY_IGNORE_Z_BASE_X_REVERSE,
            XY_IGNORE_Z_BASE_Y_FORWARD,
            XY_IGNORE_Z_BASE_Y_REVERSE,

            //            ZY вдоль оси Y - по возрастанию (игнорировать X)
            ZY_IGNORE_X_BASE_Y_FORWARD,
            //            ZY вдоль оси Y - по убыванию (игнорировать X)
            ZY_IGNORE_X_BASE_Y_REVERSE,
            //            ZX вдоль оси X - по возрастанию (игнорировать Y)
            ZX_IGNORE_Y_BASE_X_FORWARD,
            //            ZX вдоль оси X - по убыванию (игнорировать Y)
            ZX_IGNORE_Y_BASE_X_REVERSE,
        };
    };


    cXYZBeamer(const QList<sCoordsSet *> &set, sSortType::eSortType esort, bool addXYZString);

    void
    beam(const QList<sCoordsSet *> &set, bool addXYZString);
    const QString
    result() const { return result_; }

    const QList<QVector<double> > &
    vectorResult() const { return vectorResult_; }


public slots:
    //  void sort( sSortType::eSortType sortMode , QList<sCoordsSet *> &set );


private:
    QList<QVector<double> > vectorResult_;
    QString result_;
};

enum eAxisName {
    XAXIS,
    YAXIS,
    ZAXIS
};


class cBucket
{
public:
    struct sSortType {
        enum eSortType {
            ASC,
            DESC
        };
    };

    QList<sCoordsSet *>
    sort(sSortType::eSortType direction,
      eAxisName               sortableAxis
    );
    void
    add(sCoordsSet * node){ bucketNodes_.append(node);  }


private:
    QList<sCoordsSet *> bucketNodes_;
};


class cBucketCan
{
public:
    cBucketCan(QList<sCoordsSet *>     nodes,
      cXYZBeamer::sSortType::eSortType sortType);

    QList<sCoordsSet *>
    result(){ return result_; }

private:
    cBucketCan(){ }

    QList<sCoordsSet *>
    zAxisSpecialSort(QList<sCoordsSet *> nodes,
      cXYZBeamer::sSortType::eSortType   sortType);

    QList<sCoordsSet *>
    xyAxisSpecialSort(QList<sCoordsSet *> nodes,
      cXYZBeamer::sSortType::eSortType    sortType);

    QList<sCoordsSet *>
    zyzxAxisSpecialSort(QList<sCoordsSet *> nodes,
      cXYZBeamer::sSortType::eSortType      sortType);
    QList<sCoordsSet *> result_;
};


#endif // TEXTPARSER_H
