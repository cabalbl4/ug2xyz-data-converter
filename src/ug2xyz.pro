#-------------------------------------------------
#
# Project created by QtCreator 2014-04-11T19:41:33
#
#-------------------------------------------------

QT       += core gui widgets printsupport


#Header and library in subfolders. $$PWD points to project folder
LIBS += -L$$PWD/bin/
DEPENDPATH += $$PWD/bin

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ug2xyz
TEMPLATE = app


SOURCES += main.cpp\
    aboutdialog.cpp \
        cmainwindow.cpp \
    graphswidget.cpp \
    saveformatdialog.cpp \
    textparser.cpp \
    precision.cpp \
    qcustomplot/qcustomplot.cpp

HEADERS  += cmainwindow.h \
    aboutdialog.h \
    graphswidget.h \
    saveformatdialog.h \
    textparser.h \
    precision.h \
    qcustomplot/qcustomplot.h

FORMS    += cmainwindow.ui \
    aboutdialog.ui \
    graphswidget.ui \
    saveformatdialog.ui

RESOURCES += \
    resources.qrc

TRANSLATIONS = \
    i18n/ug2xyz_ru.ts \
    i18n/ug2xyz_ua.ts \
    i18n/ug2xyz_en.ts
