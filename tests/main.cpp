#include <stdexcept>
#include <QtTest/QtTest>
#include <QObject>
#include <QString>
#include <QFile>
#include <QStringList>
#include <QDebug>

#include "main.h"
#include "../src/textparser.h"

void
TestUG2XYZ::toUpper()
{
    QString str = "Hello";

    QCOMPARE(str.toUpper(), QString("HELLO"));
}

void
TestUG2XYZ::XaxisForwardSortXYZ()
{
    QFile file(":/data/Ось X.txt");
    QFile resultFile(":/data/Ось X.xyz");

    if (!file.open(QIODevice::ReadOnly) ) {
        throw std::runtime_error("Ось X.txt not opening");
    }
    if (!resultFile.open(QIODevice::ReadOnly) ) {
        throw std::runtime_error("Ось X.xyz not opening");
    }
    QTextStream stream(&file);

    stream.setCodec("Windows-1251");
    QStringList all;

    QTextStream resultStream(&resultFile);

    resultStream.setCodec("UTF-8");
    QString result = resultStream.readAll();

    while (true) {
        QString line = stream.readLine();
        if (line.isNull() )
            break;
        else
            all.append(line);
    }
    cTextParser parser(all);
    cXYZBeamer b(parser.getSet(), cXYZBeamer::sSortType::XFORWARD, true);

    QCOMPARE(b.result(), result);
}

void
TestUG2XYZ::ZaxisXmain()
{
    QFile file(":/data/Ось Z oпорная ось X.txt");
    QFile resultFile(":/data/Ось Z oпорная ось X.xyz");

    if (!file.open(QIODevice::ReadOnly) ) {
        throw std::runtime_error("Ось Y.txt not opening");
    }
    if (!resultFile.open(QIODevice::ReadOnly) ) {
        throw std::runtime_error("Ось Y.xyz not opening");
    }
    QTextStream stream(&file);

    stream.setCodec("Windows-1251");
    QStringList all;

    QTextStream resultStream(&resultFile);

    resultStream.setCodec("UTF-8");
    QString result = resultStream.readAll();

    while (true) {
        QString line = stream.readLine();
        if (line.isNull() )
            break;
        else
            all.append(line);
    }
    cTextParser parser(all);
    cXYZBeamer b(parser.getSet(), cXYZBeamer::sSortType::ZFORWARDBASEX, true);

    QCOMPARE(b.result(), result);
}

void
TestUG2XYZ::DifferentFormat()
{
    QFile file(":/data/Точки выбраны по одной.txt");
    QFile resultFile(":/data/Точки выбраны по одной.xyz");

    if (!file.open(QIODevice::ReadOnly) ) {
        throw std::runtime_error("Точки выбраны по одной.txt not opening");
    }
    if (!resultFile.open(QIODevice::ReadOnly) ) {
        throw std::runtime_error("Точки выбраны по одной.xyz not opening");
    }
    QTextStream stream(&file);

    stream.setCodec("Windows-1251");
    QStringList all;

    QTextStream resultStream(&resultFile);

    resultStream.setCodec("UTF-8");
    QString result = resultStream.readAll();

    while (true) {
        QString line = stream.readLine();
        if (line.isNull() )
            break;
        else
            all.append(line);
    }
    cTextParser parser(all);
    cXYZBeamer b(parser.getSet(), cXYZBeamer::sSortType::NOSORT, true);

    QCOMPARE(b.result(), result);
}

void
TestUG2XYZ::NewFormat()
{
    QFile file(":/data/Точки выбраны через выбрать все.txt");
    QFile resultFile(":/data/Точки выбраны через выбрать все.xyz");

    if (!file.open(QIODevice::ReadOnly) ) {
        throw std::runtime_error("Точки выбраны через выбрать все.txt not opening");
    }
    if (!resultFile.open(QIODevice::ReadOnly) ) {
        throw std::runtime_error("Точки выбраны через выбрать все.xyz not opening");
    }
    QTextStream stream(&file);

    stream.setCodec("Windows-1251");
    QStringList all;

    QTextStream resultStream(&resultFile);

    resultStream.setCodec("UTF-8");
    QString result = resultStream.readAll();

    while (true) {
        QString line = stream.readLine();
        if (line.isNull() )
            break;
        else
            all.append(line);
    }
    cTextParser parser(all);
    cXYZBeamer b(parser.getSet(), cXYZBeamer::sSortType::NOSORT, true);

    QCOMPARE(b.result(), result);
}

QTEST_MAIN(TestUG2XYZ)
