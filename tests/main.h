#ifndef MAIN_H
#define MAIN_H
#include <QtTest/QtTest>
#include <QObject>

class TestUG2XYZ : public QObject
{
    Q_OBJECT
private slots:
    void
    toUpper();
    void
    XaxisForwardSortXYZ();
    void
    ZaxisXmain();
    void
    DifferentFormat();
    void
    NewFormat();
};


#endif // MAIN_H
