QT += widgets testlib

HEADERS += \
     main.h \
     $$PWD/../src/textparser.h \
     $$PWD/../src/precision.h \
     $$PWD/../src/saveformatdialog.h

SOURCES += \
    main.cpp \
     $$PWD/../src/precision.cpp \
     $$PWD/../src/textparser.cpp \
      $$PWD/../src/saveformatdialog.cpp


FORMS  += $$PWD/../src/saveformatdialog.ui


# install
target.path = $$[QT_INSTALL_EXAMPLES]/qtestlib/tutorial1
INSTALLS += target

RESOURCES += \
    data/data.qrc
